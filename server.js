require('dotenv').config();

// importa el paquete express
const express = require('express');
/* convención. Inicializa el framework express */
const app = express();
// Permite que el body llegue como JSON (si no llega como undefined)
app.use(express.json());

const port = process.env.PORT || 3000;

const io = require('./io');
const userController = require('./controllers/UserController.js');
const authController = require('./controllers/AuthController.js');
const accountController = require('./controllers/AccountController.js');

var enableCORS = function(req, res, next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS);

app.listen(port);
console.log("API escuchando en el puerto: "+port);

app.get("/apitechu/v1/hello",
  function (req, res){
    console.log("GET /apitechu/v1/hello");
    //res.send('{"msg":"Hola desde APITechU"}');
    res.send({"msg":"Hola desde APITechU"});
  }
);


app.get("/apitechu/v1/users",userController.getUsersV1);
app.get("/apitechu/v2/users",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUserByIdV2);

app.post("/apitechu/v1/users",userController.postUsersV1);
app.post("/apitechu/v2/users",userController.postUsersV2);

app.delete("/apitechu/v1/users/:userID",userController.deleteUsersV1);
app.delete("/apitechu/v2/users/:userID",userController.deleteUsersV2);

app.post("/apitechu/v1/login",authController.loginV1);
app.post("/apitechu/v2/login",authController.loginV2);
app.post("/apitechu/v1/logout/:userID",authController.logoutV1);
app.post("/apitechu/v2/logout/:userID",authController.logoutV2);


app.get("/apitechu/v2/accounts/:userID",accountController.getAccountsOfUserV2);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("params:");
    console.log(req.params);
    console.log("query string:");
    console.log(req.query);
    console.log("headers:");
    console.log(req.headers);
    console.log(JSON.stringify(req.headers));
    console.log("body:");
    console.log(req.body);
    res.send({"msg":"OK"});
  }
);
