const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrh11ed/collections/";
const mLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

function getAccountsOfUserV2(req, res){
  var userID = req.params.userID;
  console.log("GET /apitechu/v2/accounts/"+userID);
  // build query
  var query = 'q={"userID":'+userID+'}';
  console.log("query:"+query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account?"+query+"&"+mLabAPIKey,
    function(err, resMLab, bodyRes){
      if(err){
        res.status(500).send({"msg":"Error gathering accounts of user "+userID,"error":err});
      }else{
        res.send(bodyRes);
      }
    }
  )

}

module.exports.getAccountsOfUserV2=getAccountsOfUserV2;
