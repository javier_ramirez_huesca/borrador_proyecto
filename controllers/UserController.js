const requestJson = require('request-json');
const io = require('../io');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrh11ed/collections/";
const mLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

const crypt = require('../crypt');

function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");

  var ret = {
      "users":io.gatherUserList()
  };

  if(req.query.$count=="true"){
    ret.count=ret.users.length;
  }
  if(req.query.$top){
    ret.users = ret.users.slice(0,req.query.$top);
  }
  res.send(ret);
}

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('cliente creado');
  httpClient.get("user?"+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err?body:{
        "msg":"Error gathering users"
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = 'q={"id":'+id+'}';
  console.log("query:"+query);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('cliente creado');
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = !err?body[0]:{
          "msg":"Error gathering user"
        }
        res.status(500);
      }else{
        if(body.length>0){
          var response = body[0];
        }else{
          var response = {
            "msg":"user not found"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  )
}


function postUsersV1(req, res){
  console.log("POST /apitechu/v1/users");
  var newUser = req.body;

  console.log(newUser);

  var users = io.gatherUserList();
  //users[users.length]=newUser;
  users.push(newUser);
  console.log(users);

  io.persistUserList(users);

  res.send({"msg":"New User added to file"});
}


function postUsersV2(req, res){
  console.log("POST /apitechu/v2/users");
  // don't take directly req.body to avoid insert not desired information
  var newUser = {
    "id":req.body.id,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":crypt.hash(req.body.password)
  };
  console.log(newUser);

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("user?"+mLabAPIKey, newUser,
    function(err,resMLab, body) {
      console.log("User stored");
      res.status(201).send({"msg":"User created"});
    }
  );
/*

  var users = io.gatherUserList();
  //users[users.length]=newUser;
  users.push(newUser);
  console.log(users);

  io.persistUserList(users);
*/
  //res.send({"msg":"New User added to file"});
}

function deleteUsersV1(req, res){
  console.log("DELETE /apitechu/v1/users");
  var users = io.gatherUserList();
  //var userIndex = removeFromUsersAlt1(users, req.params.userID);
  //var userIndex = removeFromUsersAlt3(users, req.params.userID);
  //var userIndex = removeFromUsersAlt2(users, req.params.userID); // this function removes the element if found
  var userIndex = removeFromUsersAlt4(users, req.params.userID);
  //var userIndex = removeFromUsersAlt5(users, req.params.userID);
  console.log("User index:"+userIndex);
  if(userIndex>=0){
    users.splice(userIndex,1);
    io.persistUserList(users);
    res.send({"msg":"User ("+req.params.userID+") deleted"});
  }else{
    res.send({"msg":"User ("+req.params.userID+") not found"});
  }
}

function deleteUsersV2(req, res){
  console.log("DELETE /apitechu/v2/users/"+req.params.userID);

  var query = 'q={"id":'+req.params.userID+'}';
  console.log("query:"+query);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.put("user?"+query+"&"+mLabAPIKey, [] /* bodyRequest, use empty change to perform a delete*/,
    function(err, resMLab, bodyRes){
      console.log(resMLab);
      console.log(bodyRes);
      if(err){
        var response = {
          "msg":"Error removing user",
          "errorDetail":err
        }
        res.status(500);
      }else{
        console.log(bodyRes);
        if(bodyRes.removed==1){
          res.status(200);
          response = {"msg":"user succesfully removed"};
        }else{
          res.status(404);
          response = {"msg":"user to remove not found"};
        }

      }

      res.send(response);
    }
  )


}


function removeFromUsersAlt1(users,userID){
  for(var i=0;i<users.length;i++){
    if(users[i].id==userID) return i;
  }
  return -1;
}

function removeFromUsersAlt2(users,userID){
  users.forEach(function(user,userIndex,users){
    if(user.id==userID){
      //console.log("userIndex"+userIndex);
      users.splice(userIndex,1);
    }
  });
  return -2;
}


function removeFromUsersAlt3(users,userID){
  for(var prop in users){
    //console.log(`users.${prop} = ${users[prop]}`);
    if(users[prop].id==userID) return prop;
  }
  return -1;
}

function removeFromUsersAlt4(users,userID){
  for(var [userIndex,user] of users.entries()){
    if(user.id==userID) {
      return userIndex;
    }
  }
  return -1;
}

function removeFromUsersAlt5(users,userID){
  return users.findIndex(function (user){
    return (user.id==userID);
  })
}

module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.getUserByIdV2=getUserByIdV2;
module.exports.postUsersV1=postUsersV1;
module.exports.postUsersV2=postUsersV2;
module.exports.deleteUsersV1=deleteUsersV1;
module.exports.deleteUsersV2=deleteUsersV2;
