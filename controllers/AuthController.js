const requestJson = require('request-json');
const io = require('../io');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrh11ed/collections/";
const mLabAPIKey = "apiKey="+process.env.MLAB_API_KEY;

const crypt = require('../crypt');


function loginV1(req,res){
  console.log("POST /apitechu/v1/login");
  // get user list
  var users = io.gatherUserList();
  // search user by email
  var user;
  for(var i=0;i<users.length;i++){
    if(users[i].email==req.body.email){
        user = users[i];
        break;
    }
  }
  if(user==null){
    res.send({"msg":"FAIL. user not found"});
    return;
  }
  // check password
  if(user.password!=req.body.password){
    res.send({"msg":"FAIL. password not valid"});
    return;
  }
  // add logged mark
  user.logged = true;
  io.persistUserList(users);
  res.send({"msg":"login ok","userID":user.id});

}

function loginV2(req,res){
  console.log("POST /apitechu/v2/login");

  // build query for finding user by email
  var query = 'q={"email":"'+req.body.email+'"}';
  console.log("query:"+query);

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err,resMLab,bodyRes){
      if(err){
        res.status(500).send({"msg":"Error gathering user","detail":err});
      }else{
        console.log(bodyRes);
        if(bodyRes.length>0){
          // user found
          var user = bodyRes[0];
          // check password
          if(crypt.checkPassword(req.body.password,user.password)){
            // password ok
            console.log("pwd ok");
            // update logged field
            // {"$set":{"logged":true}}
            httpClient.put("user?"+query+"&"+mLabAPIKey,{"$set":{"logged":true}} /* bodyPut, change to be applied */,
              function(err, resMLabPUT, bodyResPUT){ // DON'T REUSE SAME NAMES (OVERRIDE)
                if(err){
                  res.status(500).send({
                    "msg":"Error adding logged field",
                    "errorDetail":err
                  });
                }else{
                  console.log("updated");
                  console.log(bodyResPUT);
                  // TODO check n:1
                  res.status(200).send({"msg":"login ok","userID":user.id});
                }
              }
            );
          }else{
            // 401 : Unauthorized
            res.status(401).send({"msg":"FAIL. password not valid"});
          }
        }else{
          res.status(401).send({"msg":"FAIL. user not found"});
        }
      }
    }
  );
}

function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout/:userID");
  // get user list
  var users = io.gatherUserList();
  // search user by id
  var user;
  for(var i=0;i<users.length;i++){
    if(users[i].id==req.params.userID){
        user = users[i];
        break;
    }
  }
  if(user==null){
    res.send({"msg":"FAIL. user not found"});
    return;
  }
  // check logged
  if(!user.logged){
    res.send({"msg":"FAIL. User not currently logged"});
    return;
  }
  // remove logged mark
  delete user.logged;
  io.persistUserList(users);
  res.send({"msg":"logout ok","userID":user.id});

}

function logoutV2(req,res){
  console.log("POST /apitechu/v2/logout/:userID");
  // build query for finding user by email
  var query = 'q={"id":'+req.params.userID+'}';
  console.log("query:"+query);

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err,resMLab,bodyRes){
      if(err){
        res.status(500).send({"msg":"Error gathering user","detail":err});
      }else{
        if(bodyRes.length>0){
          //user found
          var user=bodyRes[0];
          // check logged
          if(user.logged){
            // update to remove logged field
            httpClient.put("user?"+query+"&"+mLabAPIKey,{"$unset":{"logged":null}} /* bodyPut, change to be applied */,
              function(err, resMLabPUT, bodyResPUT){
                if(err){
                  res.status(500).send({
                    "msg":"Error removing logged field",
                    "errorDetail":err
                  });
                }else{
                  console.log("removed logged");
                  console.log(bodyResPUT);
                  // TODO check n:1
                  res.status(200).send({"msg":"logout ok","userID":user.id});
                }
              }
            );
          }else{
            // user not logged -> return ok
            res.send({"msg":"FAIL. User not currently logged"});
          }
        }else{
          res.send({"msg":"FAIL. user not found"});
        }
      }
  });
}

module.exports.loginV1=loginV1;
module.exports.loginV2=loginV2;
module.exports.logoutV1=logoutV1;
module.exports.logoutV2=logoutV2;
