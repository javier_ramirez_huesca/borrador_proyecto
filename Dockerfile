# Dockerfile

# Source image
FROM node

# Root folder
WORKDIR /apitechu

# Copy source files from local to image
# '.' references to Dockerfile location (or where build command is executed?)
ADD . /apitechu

# Dependencies installation
# NOTE: RUN commands are executed on deploy phase, CMD commands are post-installation
RUN npm install --only=prod

# expose port
EXPOSE 3000

# Initialization command
CMD ["node", "server.js"]
