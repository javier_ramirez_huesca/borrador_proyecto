const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
const BASE_URL = 'http://localhost:3000/apitechu/v2';

describe("Test Suite Login/Logout V2",
  function(){
    var newUser=15;
    it("DELETE user id=1000", function(done) {
      chai.request(BASE_URL)
        .delete('/users/1000')
        .end(
          function(err,res){
            res.should.have.status(200);
            //res.body.msg.should.be.eql("FAIL. user not found");
            done();
          }
        )
    }),
    it("Login Invalid User", function(done) {
      chai.request(BASE_URL)
        .post('/login')
        .send({
          "email": "xx@xx.xx",
          "password": "xxx"
        })
        .end(
          function(err,res){
            //console.log(newUser);
            newUser=25;
            res.should.have.status(401);
            res.body.msg.should.be.eql("FAIL. user not found");
            done();
          }
        )
    }),
    it("Create New User (id=1000)", function(done) {
      chai.request(BASE_URL)
        .post('/users')
        .send({
              "id": 1000,
              "first_name": "Nuevo",
              "last_name": "Usuario",
              "email": "nuevo.usuario@bbva.com",
              "password":"12345"
        })
        .end(
          function(err,res){
            res.should.have.status(201);
            res.body.msg.should.be.eql("User created");
            done();
          }
        )
    }),
    it("Login Invalid Password", function(done) {
      chai.request(BASE_URL)
        .post('/login')
        .send({
          "email": "nuevo.usuario@bbva.com",
          "password": "xxx"
        })
        .end(
          function(err,res){
            //console.log(newUser);
            res.should.have.status(401);
            res.body.msg.should.be.eql("FAIL. password not valid");
            done();
          }
        )
    }),
    it("Login OK", function(done) {
      chai.request(BASE_URL)
        .post('/login')
        .send({
          "email": "nuevo.usuario@bbva.com",
          "password": "12345"
        })
        .end(
          function(err,res){
            res.should.have.status(200);
            res.body.msg.should.be.eql("login ok");
            res.body.userID.should.be.eql(1000);
            done();
          }
        )
    }),
    it("Logout OK", function(done) {
      chai.request(BASE_URL)
        .post('/logout/1000')
        .end(
          function(err,res){
            res.should.have.status(200);
            res.body.msg.should.be.eql("logout ok");
            res.body.userID.should.be.eql(1000);
            done();
          }
        )
    });
  }
)
