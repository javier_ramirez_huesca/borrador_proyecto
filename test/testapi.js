const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
/*
describe("First test",
  function(){
    it("Test that DuckDuckGo works", function(done) {
      chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function(err,res){
            console.log("Request finished");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);
            done();
          }
        )
    });
  }
)
*/

describe("Test Suite /apitechu/v1/users",
  function(){
    it("Ping", function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err,res){
            //console.log("Request finished");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde APITechU");
            done();
          }
        )
    }),
    it("GET users", function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err,res){
            //console.log("Request finished");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);
            res.body.users.should.be.a("array");
            for(user of res.body.users){
              user.should.have.property('first_name');
              user.should.have.property('email');
            }
            done();
          }
        )
      }),
      it("GET users count", function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users?$count=true')
          .end(
            function(err,res){
              //console.log("Request finished");
              //console.log(res);
              //console.log(err);
              res.should.have.status(200);
              res.body.should.have.property('count');
              res.body.count.should.be.a('Number');
              done();
            }
          )
    });
  }
)
