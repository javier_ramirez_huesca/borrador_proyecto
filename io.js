const fs = require('fs');

function persistUserList(users){
  fs.writeFile("./usuarios.json", JSON.stringify(users), 'utf8',
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log('userlist written to file');
      }
    }
    );
}

function gatherUserList(){
  return require('./usuarios.json');
}

module.exports.persistUserList = persistUserList;
module.exports.gatherUserList = gatherUserList;
